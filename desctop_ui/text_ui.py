import sys

from PySide2.QtWidgets import QApplication

from elements.win_text import TextWindow

if __name__ == "__main__":
    app = QApplication(sys.argv)

    window = TextWindow()
    window.show()

    app.exec_()
