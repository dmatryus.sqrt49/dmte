from pathlib import Path
from typing import Callable

from PySide2.QtWidgets import QMainWindow, QAction, QFileDialog, QMdiSubWindow
from PySide2.QtGui import QIcon

from elements.te_main import MainTextEdit
from elements.sources.strings_sources import APP_NAME, win_text
from elements.utils.key_manager import KeyManager
from elements.win_keymap import KeymapWindow

SOURCE_PATH = Path(__file__).absolute().parent / "sources"


class TextWindow(QMainWindow):
    def _init_menu_bar(self):
        def read_file():
            file_path, _ = QFileDialog.getOpenFileName(
                self, "Select file", str(Path.home()), "*"
            )
            if file_path:
                path = Path(file_path)
                self.mte.read_file(path)

        def save_as():
            file_path, _ = QFileDialog.getSaveFileName(
                self, "Save file", str(Path.home()), "*"
            )
            if file_path:
                path = Path(file_path)
                self.mte.save_file(path)

        def save():
            self.mte.save_file()

        def init_action(menu: str, action: str, func: Callable):
            icon_path = str(
                SOURCE_PATH
                / "icons"
                / win_text["menu"][menu]["actions"][action]["icon"]
            )
            action = QAction(
                icon=QIcon(icon_path),
                text=win_text["menu"][menu]["actions"][action]["title"],
                parent=self,
            )
            action.triggered.connect(func)
            menus[menu].addAction(action)

        def keymap():
            self.w_keymap = KeymapWindow()
            return self.w_keymap.show()

        menu_bar = self.menuBar()
        menus = {m: menu_bar.addMenu(win_text["menu"][m]["title"]) for m in win_text["menu"]}

        init_action("file", "open", read_file)
        init_action("file", "save", save)
        init_action("file", "save_as", save_as)

        # init_action("settings", "key_map", keymap)

    def __init__(self):
        super().__init__()
        self.mte = MainTextEdit()
        self.setWindowTitle(APP_NAME)

        self.setCentralWidget(self.mte)

        self._init_menu_bar()

        self.row_number_state = 0

        self.key_manager = KeyManager()
