from pathlib import Path

from charset_normalizer import from_path

from PySide2.QtWidgets import QTextEdit
from PySide2.QtGui import QPalette, QFontMetricsF

from dmte.lex import PygmentsLexerSelector
from elements.sh_main import PygmentsHighlighter
from elements.utils.style_manager import PygmentsStyleManager


class MainTextEdit(QTextEdit):
    def set_style(self, style_name: str):
        self.style_manager = PygmentsStyleManager(style_name)

        self.highlighter.setStyleManager(self.style_manager)

        pl = self.palette()
        pl.setColor(QPalette.Base, self.style_manager.background_color)
        self.setPalette(pl)

    def set_tabulation(self, size: int):
        font = self.font()
        fontMetrics = QFontMetricsF(font)
        spaceWidth = fontMetrics.width(' ')
        self.setTabStopDistance(spaceWidth * size)

    def __init__(self):
        super().__init__()
        self.block_count_state = 0
        self.style_manager = None
        self.lexer = PygmentsLexerSelector()
        self.highlighter = PygmentsHighlighter(self.document(), self.lexer)
        self.current_path = None

        self.set_style('inkpot')
        self.set_tabulation(4)

    def read_file(self, path: Path):
        try:
            self.lexer.autoselect_lexer(path)
        except:
            pass

        self.current_path = path
        result = from_path(str(path)).best()
        result = str(result)
        self.setPlainText(result)

    def save_file(self, path: Path = None):
        if path:
            with open(path, 'w') as f:
                f.write(self.toPlainText())
        elif self.current_path:
            with open(self.current_path, 'w') as f:
                f.write(self.toPlainText())
