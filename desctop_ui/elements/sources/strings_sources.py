APP_NAME = "dmte"

win_text = {
    "menu": {
        "file": {
            "title": "File",
            "actions": {
                "open": {"title": "Open file", "icon": "file.png"},
                "save": {"title": "Save", "icon": "compact-disk.png", "shortcut": "ctrl+s"},
                "save_as": {"title": "Save as...", "icon": "compact-disk.png", "shortcut": "ctrl+shift+s"},
            }},
        # "settings": {
        #     "title": "Settings",
        #     "actions": {
        #         "key_map": {"title": "Key map", "icon": "file.png"}
        #     }}
    }
}
