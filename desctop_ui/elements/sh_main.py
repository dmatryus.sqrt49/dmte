from PySide2.QtGui import QSyntaxHighlighter, QTextCursor, QTextCharFormat, QBrush, QColor, QTextDocument
from PySide2.QtWidgets import QTextEdit


class PygmentsHighlighter(QSyntaxHighlighter):

    def __init__(self, parent, lexer=None, style_manager=None):
        super().__init__(parent)
        self.lexer = lexer
        self.style_manager = style_manager

    def setLexer(self, lexer):
        self.lexer = lexer

    def setStyleManager(self, style_manager):
        self.style_manager = style_manager

    def highlightBlock(self, text):
        def get_current_tokens():
            result = []
            t_text = ""
            for i in range(len(full_tokens) - 1, -1, -1):
                t_text += full_tokens[i][1]
                result.append(full_tokens[i])
                if len(t_text) >= len(text):
                    return [result[-j] for j in range(len(result))]

        if self.lexer and self.lexer.lexer:
            current_position = self.currentBlock().position()
            p_text = self.parent().toPlainText()[-1000:current_position]
            full_text = p_text + text
            full_tokens = list(self.lexer.get_tokens(full_text))
            current_tokens = get_current_tokens()[1:]

            index = 0
            for token, tt in current_tokens:
                l = len(tt.replace('\n', ''))
                qt_format = self.style_manager.qt_formats.get(token)
                if qt_format is not None:
                    self.setFormat(index, l, qt_format)
                index += l
