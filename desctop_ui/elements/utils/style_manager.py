from abc import ABC

from PySide2.QtGui import QTextCharFormat, QPen, QColor, QFontDatabase, QBrush, QFont
from colour import Color

from dmte.style import Style, get_pygments_style


def convert_pygments_color(color: str) -> Color:
    if color:
        return Color(f"#{color}") if color[0] != "#" else Color(color)


def convert_color_to_qt(in_color):
    r, g, b = Color(in_color).get_rgb()
    r, g, b = int(r * 255), int(g * 255), int(b * 255)
    return QColor(r, g, b)


def init_qt_format(
    color: Color = None,
    bgcolor: Color = None,
    bold: bool = False,
    italic: bool = False,
    underline: bool = False,
    sans: bool = False,
    roman: bool = False,
    mono: bool = False,
    parrent=None,
):

    fmt = QTextCharFormat()
    if color:
        fmt.setForeground(QBrush(convert_color_to_qt(color)))
    else:
        fmt.setForeground(QBrush(QColor(0, 0, 0)))

    if bgcolor:
        fmt.setBackground(QBrush(convert_color_to_qt(bgcolor)))
    elif parrent:
        fmt.setBackground(QBrush(parrent.background_color))
    else:
        fmt.setForeground(QBrush(QColor(255, 255, 255, 100)))

    if bold:
        fmt.setFontWeight(QFont.Bold)
    else:
        fmt.setFontWeight(QFont.Normal)

    fmt.setFontItalic(italic)

    if underline:
        fmt.setUnderlineStyle(QTextCharFormat.SingleUnderline)
    else:
        fmt.setUnderlineStyle(QTextCharFormat.NoUnderline)

    if sans:
        fmt.setFontStyleHint(QFont.SansSerif)
    else:
        fmt.setFontStyleHint(QFont.AnyStyle)

    if roman:
        fmt.setFontStyleHint(QFont.Times)
    else:
        fmt.setFontStyleHint(QFont.AnyStyle)

    if mono:
        fmt.setFontStyleHint(QFont.TypeWriter)
    else:
        fmt.setFontStyleHint(QFont.AnyStyle)
    # elif border:
    #     # Borders are normally used for errors. We can't do a border
    #     # so instead we do a wavy underline
    #     fmt.setUnderlineStyle(
    #         QTextCharFormat.WaveUnderline)
    #     fmt.setUnderlineColor(self._get_color(value))
    return fmt


class StyleManager:
    def __init__(self, style: Style):
        pass


class PygmentsStyleManager:
    def init_qt_formats(self):

        for k in self.style.styles.keys():
            pygments_dict = self.style.style_for_token(k)
            self.qt_formats[k] = init_qt_format(
                convert_pygments_color(pygments_dict.get("color")),
                convert_pygments_color(pygments_dict.get("bgcolor")),
                pygments_dict.get("bold"),
                pygments_dict.get("italic"),
                pygments_dict.get("underline"),
                pygments_dict.get("sans"),
                pygments_dict.get("roman"),
                pygments_dict.get("mono"),
                self,
            )

    def __init__(self, style_name: str):
        self.style_name = style_name
        self.style = get_pygments_style(self.style_name)
        self.background_color = convert_color_to_qt(
            convert_pygments_color(self.style.background_color)
        )

        self.qt_formats = {}
        self.init_qt_formats()
