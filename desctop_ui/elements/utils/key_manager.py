from typing import Dict

from PySide2.QtCore import Qt


class KeyManager:
    KEY_MAPPING: Dict

    def _init_keymap(self):
        self.KEY_MAPPING = {v: k[3+1:] for k, v in Qt.Key.__dict__.items() if 'Key_' in k}

    def __init__(self):
        self._init_keymap()


if __name__ == '__main__':
    km = KeyManager()
    print(km.KEY_MAPPING)
