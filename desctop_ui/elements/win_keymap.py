from PySide2.QtCore import QAbstractTableModel
from PySide2.QtWidgets import QTableWidget


class TableModel(QAbstractTableModel):
    def __int__(self, header, data, parent=None):
        super().__init__(parent)
        self.header = header
        self.data = data


class KeymapWindow(QTableWidget):
    def __init__(self):
        super().__init__()

        self.setWindowTitle("Keymap")
        self.setRowCount(2)
        self.setColumnCount(2)
        # self.table_view.setModel()